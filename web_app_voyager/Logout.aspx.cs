﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using web_app_voyager.Constants;

namespace web_app_voyager
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session[SessionManager.USER_SESSION] = null;
            Response.Redirect("Login.aspx");
        }
    }
}
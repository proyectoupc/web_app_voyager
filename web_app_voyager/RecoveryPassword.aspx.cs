﻿using Newtonsoft.Json;
using System;
using web_app_voyager.Constants;
using web_app_voyager.Dominio;
using web_app_voyager.Utils;

namespace web_app_voyager
{
    public partial class RecoveryPassword : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String strTokenRecoveryPassword = Request.QueryString["token"];
                if (string.IsNullOrEmpty(strTokenRecoveryPassword))
                {
                    dvRestore.Visible = false;
                    dvRestoreUknow.Visible = true;
                    lblMensajeFinal.Text = "No se pudo identificar la solicitud.";
                }
                else
                {
                    hfToken.Value = strTokenRecoveryPassword;
                    User user = JsonConvert.DeserializeObject<User>(HttpWebRequestUtil.get(Constant.API_URL_USER + "/ObtainUserbyTokenReocveryPassword/" + strTokenRecoveryPassword));
                    if (user != null)
                    {
                        dvRestore.Visible = true;
                        dvRestoreUknow.Visible = false;
                    }
                    else
                    {
                        dvRestore.Visible = false;
                        dvRestoreUknow.Visible = true;
                        lblMensajeFinal.Text = "La solicitud ya caducó, genera una nueva solicitud para restablecer su contraseña.";
                    }
                }
            }
        }

        protected void _onClick(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtPassword.Text) || String.IsNullOrEmpty(txtPasswordRecovery.Text))
            {
                lblMensaje.Text = "*Ingrese todos los campos";
            }
            else
            if (txtPassword.Text != txtPasswordRecovery.Text)
            {
                lblMensaje.Text = "La confirmación del password es incorrecto";
            }
            else
            {

                User user = JsonConvert.DeserializeObject<User>(HttpWebRequestUtil.get(Constant.API_URL_USER + "/ObtainUserbyTokenReocveryPassword/" + hfToken.Value));
                if (user == null)
                {

                    lblMensaje.Text = "Hubo problemas al restaurar la contraseña, intente más tarde";

                }
                else
                {
                    user.password = txtPassword.Text;
                    user = JsonConvert.DeserializeObject<User>(HttpWebRequestUtil.post(Constant.API_URL_USER + "/UpdatePassword", JsonConvert.SerializeObject(user).ToString()));
                    if (user != null)
                    {
                        user.token = null;
                        HttpWebRequestUtil.post(Constant.API_URL_USER + "/updateTokenRecoveryPassword", JsonConvert.SerializeObject(user).ToString());
                        dvRestore.Visible = false;
                        dvRestoreUknow.Visible = true;
                        lblMensajeFinal.Text = "La contraseña se restableció correctamente, vuelve a ingresar a la aplicación.";
                    }
                    else
                    {
                        lblMensaje.Text = "Hubo problemas al restaurar la contraseña, intente más tarde";
                    }
                }
            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecoveryPassword.aspx.cs" Inherits="web_app_voyager.RecoveryPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="Content/bootstrap.min.css" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <center><img src="Images/voyager_logo.png" style="width:150px"/></center>
            <br />
            <div class="row">
                <div class="col-md-offset-3 col-md-6 col-md-offset-3">
                    <label>Para restablecer su contraseña, por favor complete los campos asignados:</label>
                </div>
            </div>
            <div class="row ">
                <div class="col-sm-4 col-sm-offset-4" style="padding-top: 10px">
                    <div class="login-panel panel  panel-primary">
                        <div class="panel-body">
                            <div id="dvRestore" runat="server">
                                <div class="form-group">
                                    <label for="txtPassword">Ingrese nueva contraseña:*</label>
                                    <asp:TextBox ID="txtPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label for="txtPasswordRecovery">Confirme su nueva contraseña:*</label>
                                    <asp:TextBox ID="txtPasswordRecovery" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
                                </div>
                                Campos obligatorios (*)
                                <asp:HiddenField ID="hfToken" runat="server" />
                                <div class="form-group">
                                <asp:Label ID="lblMensaje" style="color:red" runat="server"></asp:Label></div>
                                <asp:Button ID="btnRecovery" class="btn btn-primary btn-lg btn-block" runat="server" Text="Restablecer contraseña" OnClick="_onClick" />
                            </div>
                            <div id="dvRestoreUknow" runat="server">
                                <asp:Label ID="lblMensajeFinal" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
            <div class="modal-dialog modal-sm">
                <div class="modal-content" style="width: 48px">
                    <span class="fa fa-spinner fa-spin fa-3x" style="color: #022843"></span>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

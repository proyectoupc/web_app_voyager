﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.UI;
using web_app_voyager.Constants;
using web_app_voyager.Domain;
using web_app_voyager.Dominio;
using web_app_voyager.Utils;

namespace web_app_voyager
{
    public partial class _Default : Page
    {
       public List<Place> places = new List<Place>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                User user = (User)Session[SessionManager.USER_SESSION];
                if (user != null)
                {
                    places = JsonConvert.DeserializeObject<List<Place>>(HttpWebRequestUtil.get(Constant.API_URL_PLACE + "/GetAll/" + user.idUser));

                }

            }

        }
    }
}
﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="web_app_voyager._Default" %>

<%@ Import Namespace="web_app_voyager.Constants" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <div class="container">
        <%
            for (int i = 0; i < places.Count; i++)
            {

        %>

        <div class="panel panel-primary">
            <div class="panel-heading" style="font-weight: bold; font-size: 20px"><%= places[i].name %></div>

            <div class="panel-body" style="font-size: 16px">
                <div class="row">
                    <div class="col-md-6">
                        <center>
                <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height:400px">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                         <% for (int z = 0; z < places[i].idPhotos.Count; z++)
                             { %>
                       <li data-target="#myCarousel" data-slide-to="<%=z %>" <%if (z == 0)
                           { %> class="active"<%} %>></li>
                        <%} %>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <% for (int z = 0; z < places[i].idPhotos.Count; z++)
                            { %>
                        <div class="item<%if (z == 0)
                            { %> active<%} %>">
                            <img src="<%=Constant.API_URL_PHOTO+"/GetPhoto/"+places[i].idPhotos[z] %>" style="height:400px">
                        </div>
                        <%} %>
                        
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                </center>
                    </div>
                    <div class="col-md-6">
                        <div id="map_<%= i%>" style="width: 100%; height: 400px">
                        </div>

                    </div>
                </div>
                <br />
                <%=places[i].description %>
                <br />
                <label style="color:#022843"> Contacto:</label> <%=places[i].contactName %>
                <br />
                <label style="color:#022843">Número telefónico:</label>  <%=places[i].contactPhone %>
            </div>
        </div>
        <br />

        <%} %>
    </div>

    <script>
        <%
        for (int i = 0; i < places.Count; i++)
        {
        %>
        var map<%=i%>;
        <%
        }
        %>
        function initMap() {
              <%
        for (int i = 0; i < places.Count; i++)
        {
        %>
            var myLatLng<%=i%> = { lat: <%= places[i].latitude%>, lng: <%= places[i].longitude%>};
            map<%=i%> = new google.maps.Map(document.getElementById('map_<%=i%>'), {
                zoom: 16,
                center: myLatLng<%=i%>
            });
            var marker<%=i%>;
            if (marker<%=i%> != undefined) {
                marker<%=i%>.setMap(null)
            }
            marker<%=i%> = new google.maps.Marker({
                position: myLatLng<%=i%>,
                map: map<%=i%>,
                title: "<%= places[i].name%>"
            });

            <%
        }
        %>
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCW_fSpTSF90-1M5zQzlS5KZJ9hqiilGHk&callback=initMap"
        async defer></script>
</asp:Content>

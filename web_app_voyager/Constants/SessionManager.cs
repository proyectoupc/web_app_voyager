﻿using System;

namespace web_app_voyager.Constants

{
    public class SessionManager
    {
        public static String USER_SESSION = "USER_SESSION";
        public static String THEME_SESSION = "THEME_SESSION";
        public static String ROL_SESSION = "ROL_SESSION";
        public static String MSGBOX_MENSAJE = "MSGBOX_MENSAJE";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_app_voyager.Constants
{
    public class Constant
    {
        private static String API_URL = "https://voyager-api.azurewebsites.net";
        public static String API_URL_USER = API_URL + "/WSUser.svc";
        public static String API_URL_PLACE = API_URL + "/WSPlace.svc";
        public static String API_URL_COMMENT = API_URL + "/WSComment.svc";
        public static String API_URL_FOLLOWER = API_URL + "/WSFollower.svc";
        public static String API_URL_PHOTO = API_URL + "/WSPhoto.svc";
        public static String API_URL_RATING_PLACE = API_URL + "/WSRatingPlace.svc";
        public static String API_URL_SAVE_PLACE = API_URL + "/WSSavePlace.svc";
        public static String API_URL_VIEWS_PLACE = API_URL + "/WSViewsPlace.svc";
    }
}
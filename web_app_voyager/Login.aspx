﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="web_app_voyager.Login" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login - VOYAGER</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="Content/bootstrap.min.css" />
    <link href="Content/bootstrap-theme.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <style>
        .bd-example-modal-lg .modal-dialog {
            display: table;
            position: relative;
            margin: 0 auto;
            top: calc(50% - 24px);
        }

            .bd-example-modal-lg .modal-dialog .modal-content {
                background-color: transparent;
                border: none;
            }
    </style>
</head>
<body>
    <div class="container">
        <center><img src="Images/voyager_logo.png" style="width:150px"/></center>
        <div class="row " style="text-align: center;">
            <div class="col-sm-4 col-sm-offset-4" style="padding-top: 50px">
                <div class="login-panel panel  panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                        <form method="post" id="frm" role="form" action="/login/"
                            onsubmit="return loginValidating()">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Usuario" name="userID" id="userID" type="text"
                                        value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" id="password"
                                        type="password" value="">
                                </div>
                                <div class="form-group hide" id="idLoginFailded">
                                    <p class="text text-danger">* Usuario/Password incorrecto</p>
                                </div>
                                <div class="form-group hide" id="idDataEmpy">
                                    <p class="text text-danger">* Ingresa todos los datos</p>
                                </div>
                                <input class="btn btn-primary btn-lg btn-block" type="button" value="Ingresar" onclick="login()" />
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content" style="width: 48px">
                <span class="fa fa-spinner fa-spin fa-3x" style="color: #022843"></span>
            </div>
        </div>
    </div>

    <script>
        var peticion_http = null;

        function inicializa_xhr() {
            if (window.XMLHttpRequest) {
                return new XMLHttpRequest();
            } else if (window.ActiveXObject) {
                return new ActiveXObject("Microsoft.XMLHTTP");
            }
        }

        function loginValidating() {
            var account = $('#accountID').val();
            var userID = $('#userID').val();
            var password = $('#password').val();

            if (account == '' || userID == '' || password == '') {
                $('#idLoginFailded').attr('class', 'form-group hide');
                $('#idDataEmpy').attr('class', 'form-group show');
                return false;
            }
        }
        function login() {
            $('.modal').modal('show');
            $.ajax({
                type: 'POST',
                url: 'Login.aspx/LoginUser',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                data: JSON.stringify({ email: $('#userID').val(), password: $('#password').val() }),
                success: function (data) {
                    $('.modal').modal('hide');
                    var response = data.d;
                    if (response !== null && response != "ERROR") {
                        document.location.href = "/";
                    } else {
                        alert('No se pudo inicir sesion, verifique su usuario y contraseña');
                    }
                }, error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $('.modal').modal('hide');
                    alert("Ocurrió un error al obtener al inciar sesion");
                }
            });
        }
        function modal() {
            $('.modal').modal('show');
        }
    </script>

</body>
</html>

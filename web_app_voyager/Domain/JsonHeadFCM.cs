﻿using System;
using System.Collections.Generic;

namespace web_app_voyager.Domain
{
    public class JsonHeadFCM
    {
        public JsonDataFCM data { get; set; }
        public List<String> registration_ids { get; set; }

    }
    public class JsonDataFCM
    {
        public String my_custom_key { get; set; }
        public Boolean my_custom_key2 { get; set; }

    }
}
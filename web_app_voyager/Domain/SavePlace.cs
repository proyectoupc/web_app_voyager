﻿using System;

namespace web_app_voyager.Domain
{
    public class SavePlace
    {
        public Int64 idSavePlace { get; set; }
        public Int64 idUser { get; set; }
        public Int64 idPlace { get; set; }
        public Place place { get; set; }

    }
}
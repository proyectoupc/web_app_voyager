﻿using System;
using web_app_voyager.Dominio;

namespace web_app_voyager.Domain
{
    public class Follower
    {
        public Int64 idFollower { get; set; }
        public Int64 idUser { get; set; }
        public Int64 idUserFollower { get; set; }
        public String dayFollowed { get; set; }
        public String userName { get; set; }
        public User user { get; set; }
    }
}
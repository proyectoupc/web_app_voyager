﻿

using web_app_voyager.Dominio;

namespace web_app_voyager.Domain
{
    public class Notification
    {
        public User user { get; set; }
        public Place place { get; set; }
        public Comment comment { get; set; }
    }
}
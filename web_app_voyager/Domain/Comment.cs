﻿using System;
using web_app_voyager.Dominio;

namespace web_app_voyager.Domain
{
    public class Comment
    {
        public Int64 idComentario { get; set; }
        public Int64 idPlace { get; set; }
        public Int64 idUser { get; set; }
        public String description { get; set; }
        public String datetime { get; set; }
        public String userName { get; set; }
        public User user { get; set; }
    }
}
﻿using System;
namespace web_app_voyager.Domain
{
    public class RatingPlace
    {
        public Int64 idRatingPlace { get; set; }
        public Int64 idPlace { get; set; }
        public Int64 idUser { get; set; }
        public Int64 countLikes { get; set; }
    }
}
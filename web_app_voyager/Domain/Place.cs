﻿using System;
using System.Collections.Generic;
using web_app_voyager.Dominio;

namespace web_app_voyager.Domain
{
    public class Place
    {
        public Int64 idPlace { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String tags { get; set; }
        public String postDateTime { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public int rating { get; set; }
        public int views { get; set; }
        public Int64 idUser { get; set; }
        public User user { get; set; }
        public String contactName { get; set; }
        public String contactPhone { get; set; }
        public List<Int64> idPhotos { get; set; }
    }
}
﻿using System;

namespace web_app_voyager.Dominio
{
    public class User
    {
        public Int64 idUser { get; set; }
        public String email { get; set; }
        public String password { get; set; }
        public String name { get; set; }
        public String lastName { get; set; }
        public String birthday { get; set; }
        public String gender { get; set; }
        public String photo { get; set; }
        public String description { get; set; }
        public String backgroundPhoto { get; set; }
        public String token { get; set; }
    }
}
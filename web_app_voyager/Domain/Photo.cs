﻿using System;

namespace web_app_voyager.Domain
{
    public class Photo
    {
        public Int64 idPhoto { get; set; }
        public Int64 idPlace { get; set; }
        public String photo { get; set; }
    }
}
﻿using System;

namespace web_app_voyager.Domain
{
    public class ViewsPlace
    {
        public Int64 idViewsPlace { get; set; }
        public Int64 idPlace { get; set; }
        public Int64 idUser { get; set; }
        public Int64 countViews { get; set; }
    }
}
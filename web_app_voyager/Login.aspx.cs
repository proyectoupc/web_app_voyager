﻿using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Services;
using web_app_voyager.Constants;
using web_app_voyager.Dominio;
using web_app_voyager.Utils;

namespace web_app_voyager
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = (User)Session[SessionManager.USER_SESSION];
            if (user != null)
            {
                Response.Redirect("/");
            }
        }
        [WebMethod]
        public static String LoginUser(String email, String password)
        {
            User userLogin = new User();
            userLogin.email = email;
            userLogin.password = password;
            User user =JsonConvert.DeserializeObject<User>(HttpWebRequestUtil.post(Constant.API_URL_USER + "/Login", JsonConvert.SerializeObject(userLogin).ToString()));
            if (user != null)
            {
                HttpContext.Current.Session[SessionManager.USER_SESSION] = user;
                return "OK";
            }
            return "ERROR";
        }
    }
}
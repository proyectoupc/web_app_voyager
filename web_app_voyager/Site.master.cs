﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using web_app_voyager.Constants;
using web_app_voyager.Dominio;

namespace web_app_voyager
{
    public partial class SiteMaster : MasterPage
    {
        public String userName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = (User)Session[SessionManager.USER_SESSION];
            if (user == null)
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                userName = String.Format("{0} {1}", user.name, user.lastName);
            }
        }
       
    }
}